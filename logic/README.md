# Overview

The logic directory stores materials for business logic, such as communication between the front end and the back end.

The backend may be started by running the "flask run" command from within the ./src/app/ directory.
This will make materials available locally here: http://127.0.0.1:5000/  