from app import db, login
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

class Patient(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    fname=db.Column(db.String(140))
    lname=db.Column(db.String(140))
    def __repr__(self):
        return '<Patient {}>'.format(self.username)

class Intervention(db.Model):
    intervention_id = db.Column(db.Integer, primary_key=True, unique=True)
    intervention_name=db.Column(db.String(140))
    patient_id=(db.Integer,db.ForeignKey('Patient.id'))
    therapist_id=(db.Integer,db.ForeignKey('Therapist.id'))
    def __repr__(self):
        return '<Intervention {}>'.format(self.intervention_id)

class Therapist(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    fname=db.Column(db.String(140))
    lname=db.Column(db.String(140))
    def __repr__(self):
        return '<Therapist {}>'.format(self.username)
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)
    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

@login.user_loader
def load_user(id):
    return Therapist.query.get(int(id))
